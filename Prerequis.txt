Bibliothèques nécéssaires (il sera nécéssaire d'installer leurs dépendances) : pygame, os, ast, PySimpleGUI, keyboard, tkinter, sys, functools, abc, time, random, socket, _thread

Version de python recommandée: Python 3.7.9
Version de pip utilisée: pip 23.3.1
IDE utilisé: Thonny

Pour utiliser le mode de jeu en ligne, vous devrez :
1 - Passer par une application telle que LogMeIn Hamachi qui permet de simuler un réseau local en distanciel. Pour Hamachi, vous devrez vous connecter/créer un compte Hamachi, puis cliquer sur l'icone power sur l'application. 
2 - Créer votre réseau. Nous détaillerons ici la procédure pour Hamachi. Allez dans Réseau => Créer un réseau, puis renseignez le nom de votre réseau dans id et le mot de passe du réseau. Vous pourrez bien sûr paramétrer de manière plus précise ce réseau par la suite. 
3 - Enfin, une fois le réseau crée, l'autre joueur pourra s'y connecter en utilisant la même application. Sur Hamachi, il suffit d'aller sur Réseau => Rejoindre un réseau, et d'y rentrer les informations du réseau. 
4 - Dans projet\Build\Ip.txt, renseignez l'addresse IP de l'autre PC.

Les bibliothèques nécéssaires ont été fournies dans projet\Doc\Lib. Pour les installer vous devrez copier ce dossier dans C:\Users\VotreNomD'Utilisateur\AppData\Local\Programs\Thonny\ afin de remplacer/compléter l'ancien dossier Lib. Suivez cette procédure si vous utilisez Thonny, sinon vous devrez localiser dans votre dossier d'IDE où se trouve le dossier Lib afin de le remplacer par celui-ci.

Le projet à uniquement été testé sur Windows 11, Windows 10 et RaspberryOS.
