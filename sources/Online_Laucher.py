"""Le Online Launcher lance les programmes Host et Client"""
    
import pygame
import sys
import os

pygame.init()
screen = pygame.display.set_mode((1080, 720))


def quitting_to_main_menu():
    exit()


def button(screen, position, text):
    """Crée un/des boutons"""
    font = pygame.font.Font("./Fonts/Super Mario Maker Extended.ttf", 75)
    text_render = font.render(text, 1, (10, 10, 51))
    x, y, w , h = text_render.get_rect()
    x, y = position
    pygame.draw.line(screen, (150, 150, 200), (x, y), (x + w , y), 5)
    pygame.draw.line(screen, (150, 150, 200), (x, y - 2), (x, y + h), 5)
    pygame.draw.line(screen, (50, 50, 100), (x, y + h), (x + w , y + h), 5)
    pygame.draw.line(screen, (50, 50, 100), (x + w , y+h), [x + w , y], 5)
    pygame.draw.rect(screen, (100, 100, 150), (x, y, w , h))
    return screen.blit(text_render, (x, y))


def host():
    """lance Host.py"""
    os.system(r"python Host.py")


def client():
    """lance Client.py"""
    os.system(r"python Client.py")


def menu():
    """gère l'affichage du menu et les boutons"""
    b1 = button(screen, (350, 550), "Quitter")
    b2 = button(screen, (110, 400), "Héberger une partie")
    b3 = button(screen, (110, 250), "Rejoindre une partie")
    A = False
    while True:
        font = pygame.font.Font("./Fonts/Super Mario Maker Extended.ttf", 75)
        text_surface = font.render('Mode Multijoueur', False, (255, 189, 27))
        text_surface_2 = font.render('Mode Multijoueur', False, (0, 0, 0))
        screen.blit(text_surface_2, (140,65))
        screen.blit(text_surface, (150,75))
        if A == False:
            pygame.mixer.music.play(-1)
            A = True
        for event in pygame.event.get():
            if (event.type == pygame.QUIT):
                pygame.quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if b1.collidepoint(pygame.mouse.get_pos()):
                    pygame.quit()
                elif b2.collidepoint(pygame.mouse.get_pos()):
                    A = False
                    pygame.mixer.music.stop()
                    host()
                elif b3.collidepoint(pygame.mouse.get_pos()):
                    A = False
                    pygame.mixer.music.stop()
                    client()
        try:    
            pygame.display.update()
        except:
            quitting_to_main_menu()
    pygame.quit()
    

screen.blit(pygame.image.load("./Sprites/background/bg_1.png"), (0,0))
pygame.mixer.pre_init(44100, -16, 2, 2048)
pygame.mixer.music.load("./Music/Jeremy Blake - Powerup !.mp3")
menu()
