"""Ce programme permet de jouer aux niveaux du mode monde"""
import pygame
import os
import ast
import PySimpleGUI as sg
import keyboard
import tkinter
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import sys
from functools import wraps
from abc import ABC, abstractmethod
from time import sleep
import random


class Camera():
    """gère la caméra du joueur"""
    def __init__(self, player, screen):
        vec = pygame.math.Vector2
        self.player = player
        self.offset = vec(0, 0)
        self.offset_fl = vec(0, 0)
        self.dwidth = screen.get_width()
        self.dheight = screen.get_height()
        self.const = vec(0, 0)
    def setmeth(self, methode):
        self.methode = methode
    def scroll(self):
        self.methode.scroll()
        

class CameraScroll(ABC):
    """fais défiler la caméra selon la méthode de scrolling choisie"""
    def __init__(self, camera, player):
        self.camera = camera
        self.player = player
    @abstractmethod
    def scroll(self):
        pass


class Suivre(CameraScroll):
    """méthode de scrolling qui suit le joueur"""
    def __init__(self, camera, player):
        CameraScroll.__init__(self, camera, player)
    def scroll(self):
        self.camera.offset_fl.x += (self.player.x - self.camera.offset_fl.x + self.camera.const.x)
        self.camera.offset_fl.y += (self.player.y - self.camera.offset_fl.y + self.camera.const.y)
        self.camera.offset.y, self.camera.offset.x = int(self.camera.offset_fl.y), int(self.camera.offset_fl.x)


class Auto(CameraScroll):
    """méthode de scrolling qui avance sans suivre le joueur"""
    def __init__(self, camera, player, acc):
        self.acc = acc
        CameraScroll.__init__(self, camera, player)
    def scroll(self):
        self.player.x += 1 * self.acc
        self.camera.offset_fl.x += 1 * self.acc
        self.camera.offset_fl.y += (self.player.y - self.camera.offset_fl.y + self.camera.const.y)
        self.camera.offset.y, self.camera.offset.x = int(self.camera.offset_fl.y), int(self.camera.offset_fl.x)


class Bloc(pygame.sprite.Sprite):
    """classe qui gère les blocs"""
    def __init__(self, blocks, type_block, cx, cy, list_block_literral, rect_list, szbw):
        """initialise la classe bloc"""
        pygame.sprite.Sprite.__init__(self)
        self.type = type_block
        self.cx = cx
        self.cy = cy
        self.szbw = szbw
        self.old_img = None
        list_block_literral.append(self)
        self.lbl = list_block_literral
        self.time = 30
    def draw_bloc(self, screen, blocks_cords_only, rect_list, blocks, dx, dy):
        """dessine le bloc"""
        self.screen = screen
        self.img = pygame.image.load(self.type).convert_alpha()
        self.img = pygame.transform.scale(self.img, (self.szbw, self.szbw)).convert_alpha()
        self.old_img = self.img
        blocks_cords_only.append((self.cx, self.cy))
        blocks.append((self.cx, self.cy, self.img))
        self.rect = pygame.Rect(self.cx, self.cy, self.szbw, self.szbw)
        rect_list.append(self.rect)
    def update(self):
        pass
    def kill(self):
        """'tue' le bloc"""
        del self
    def bloc_type_check(self, target, rect_list, blocks, blocks_cords_only):
        """vérifie le type de bloc quand une entité se cogne dessus"""
        if "./Sprites/damage/" in self.type:
            if "lava" in self.type:
                target.HPManager("damage", 2)
            if "spike" or "wolf_trap" or "barbed" in self.type:
                target.HPManager("damage", 1)
        elif "./Sprites/special/" in self.type:
            if "bouncer" in self.type:
                target.vely = -100
            if "glass" in self.type:
                if target.velx <= 14:
                    target.velx *= 1.01
            if "ladder" in self.type:
                target.vely -= 10
                target.isfalling = False
                target.gothrough = True
            if "iron_crate" in self.type:
                blocks_cords_only.remove((self.cx, self.cy))
                blocks.remove((self.cx, self.cy, self.img))
                rect_list.remove(self.rect)
                if target.direction == "left":
                    self.cx -= 2
                else:
                    self.cx += 2
                self.rect = pygame.Rect(self.cx, self.cy, self.szbw, self.szbw)
                blocks_cords_only.append((self.cx, self.cy))
                blocks.append((self.cx, self.cy, self.img))
                self.rect = pygame.Rect(self.cx, self.cy, self.szbw, self.szbw)
                rect_list.append(self.rect)
            if "quicksand" in self.type:
                target.y += 10
                target.gothrough = True
            if "spawn" in self.type:
                target.HPManager("full")
            if "flag" in self.type:
                target.win = True
            if "teleporter" in self.type and "teleporter_locked" not in self.type:
                if self.time == 0:
                    temp = []
                    for elt in self.lbl:
                        if elt != self and elt.type == self.type:
                            temp.append(elt)
                    a = None
                    b = None
                    for eltt in temp:
                        if a == None:
                            a = abs(eltt.cx - self.cx + eltt.cy - self.cy)
                            b = (eltt.cx - 8 * self.szbw, eltt.cy - 10 * self.szbw)
                        else:
                            if abs(eltt.cx - self.cx + eltt.cy - self.cy) <= a:
                                a = abs(eltt.cx - self.cx + eltt.cy - self.cy)
                                b = (eltt.cx - 8 * self.szbw, eltt.cy - 10 * self.szbw)
                    if b == None:
                        b = (target.x, target.y)
                    target.x, target.y = b
                    self.time = 30
                else:
                    self.time -= 1
            if "teleporter_locked" in self.type:
                if target.items["key"] >= 1:
                    self.type = "./Sprites/special/teleporter.png"
                    self.img = pygame.image.load(self.type).convert_alpha()
                    self.img = pygame.transform.scale(self.img, (self.szbw, self.szbw)).convert_alpha()
                    self.old_img = self.img
            if "falling_platform" in self.type:
                blocks_cords_only.remove((self.cx, self.cy))
                blocks.remove((self.cx, self.cy, self.img))
                rect_list.remove(self.rect)
                self.cy += 1
                self.rect = pygame.Rect(self.cx, self.cy, self.szbw, self.szbw)
                blocks_cords_only.append((self.cx, self.cy))
                blocks.append((self.cx, self.cy, self.img))
                self.rect = pygame.Rect(self.cx, self.cy, self.szbw, self.szbw)
                rect_list.append(self.rect)
        else:
            target.gothrough = False
            if "stair" in self.type:
                target.y -= 1
                target.velx = 0
            if "empty" in self.type:
                target.gothrough = True
    def __del__(self):
        return None
    def __repr__(self):
        return f"{self.rect}, {self.type}"
    def __call__(self):
        return f"{self.rect}, {self.type}"
  
  
class Ennemy(pygame.sprite.Sprite):
    """classe qui s'occupe des ennemis"""
    def __init__(self, x, y, szbw, Type):
        """initialise les ennemis"""
        pygame.sprite.Sprite.__init__(self)
        self.type = Type
        self.gotten = False
        self.animactual = 0
        self.cooldown = 0
        self.kill = 600
        self.x = x
        self.y = y
        self.szbw = szbw
        self.ix = self.x // self.szbw
        self.iy = self.y // self.szbw
        self.cx, self.cy = self.ix * self.szbw, self.iy * self.szbw
        self.velx = 0
        self.vely = 0 
        self.isfalling = False
        self.img = pygame.image.load(self.type).convert_alpha()
        self.rect = pygame.Rect(self.cx, self.cy, self.szbw, self.szbw)
        self.anim = self.animation("idle")
        self.direction = "left"
        self.oldx = self.x
        if "jeanpascaldelaflibustiere" not in self.type:
            self.pv = 3
        else:
            self.pv = 1
        self.invisframe = 0
        self.gothrough = False
        self.timer = 120
        self.projectiles = []
        self.projectiles_index_temp = -1
    def walk(self, rect_list, list_block_literral, blocks, blocks_cords_only):
        """fais avancer les ennemis"""
        self.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only)
        if (self.x != (self.x + self.velx)):
            if self.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == True:
                self.oldx = self.x
                self.x += self.velx
            if self.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == 'r' or self.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == 'l':
                self.vely = -1
                self.velx = 0
                if self.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == 'r':
                    self.x = self.oldx + 10
                    self.oldx = self.x
                else:
                    self.x = self.oldx - 10
                    self.oldx = self.x
        if self.gothrough == True:
            self.isfalling = False
            self.y += self.vely
        if self.y != (self.y + self.vely):
            if self.vely > 0:
                self.y += self.vely
            if self.gothrough == False and self.vely < 0:
                if self.isfalling == False:
                    for elt in rect_list:
                        self.temp_rect = pygame.Rect(self.cx - 1, self.cy + 2 * self.szbw, self.szbw, self.szbw)
                        if self.temp_rect.colliderect(elt):
                            self.vely = 7
                            self.isfalling = True
                            return -1
                        self.temp_rect = pygame.Rect(self.cx + 1, self.cy + 2 * self.szbw, self.szbw, self.szbw)
                        if self.temp_rect.colliderect(elt):
                            self.vely = 7
                            self.isfalling = True
                            return -1
                        self.temp_rect = pygame.Rect(self.cx, self.cy - 7, self.szbw, self.szbw)
                        if self.temp_rect.colliderect(elt):
                            self.vely = 7
                            self.isfalling = True
                            return -1
                    self.y += self.vely
    def update(self, surface, closest_player, rect_list, dx, dy):
        """met à jour les variables des ennemis et leur fait faire une action"""
        try:
            self.temp_del = []
            if len(self.projectiles) != 0:
                for proj in self.projectiles:
                    self.projectiles_index_temp += 1
                    if proj.kill == True:
                        self.temp_del.append(self.projectiles_index_temp)
                    else:
                        proj.update_walk_die(closest_player, surface, dx, dy, rect_list)
            for indexes in self.temp_del:
                self.projectiles.pop(indexes)
            self.temp_del = []
            self.projectiles_index_temp = -1
            if self.velx == 0:
                if self.type == "./Sprites/ennemy/angel/angel.png":
                    self.img = pygame.image.load("./Sprites/empty.png").convert_alpha()
                if self.type == "./Sprites/ennemy/fly/idle/idle.png":
                    self.anim = self.animation("idle")
                if self.type == "./Sprites/ennemy/cactus/idle/idle.png":
                    self.anim = self.animation("idle")
                if self.type == "./Sprites/ennemy/turret/idle/idle.png":
                    self.anim = self.animation("idle")
                if self.type == "./Sprites/ennemy/penguini/idle/idle.png":
                    self.anim = self.animation("idle")
                if "jeanpascaldelaflibustiere" in self.type:
                    self.anim = self.animation("idle")
            elif self.velx != 0 and self.type == "./Sprites/ennemy/angel/angel.png":
                self.img = pygame.image.load(self.type).convert_alpha
            if self.velx > 0:
                self.velx -= 1
            elif self.velx < 0:
                self.velx += 1
            if self.type == "./Sprites/ennemy/angel/angel.png":
                self.methods("angel", closest_player)
            elif self.type == "./Sprites/ennemy/fly/idle/idle.png":
                self.methods("fly", closest_player)
            elif self.type == "./Sprites/ennemy/cactus/idle/idle.png":
                self.methods("cactus", closest_player)
            elif self.type == "./Sprites/ennemy/turret/idle/idle.png":
                self.methods("turret", closest_player)
            elif self.type == "./Sprites/ennemy/penguini/idle/idle.png":
                self.methods("penguini", closest_player)
            elif self.type == "./Sprites/ennemy/jeanpascaldelaflibustiere/phase1/idle/idle.png":
                self.methods("jp1", closest_player)
            elif self.type == "./Sprites/ennemy/jeanpascaldelaflibustiere/phase2/idle/idle.png":
                self.methods("jp2", closest_player)
            elif self.type == "./Sprites/ennemy/jeanpascaldelaflibustiere/phase3/idle/idle.png":
                self.methods("jp3", closest_player)
            elif self.type == "./Sprites/ennemy/jeanpascaldelaflibustiere/phase4/idle/idle.png":
                self.methods("jp4", closest_player)
            elif self.type == "./Sprites/ennemy/jeanpascaldelaflibustiere/phase5/idle/idle.png":
                self.methods("jp5", closest_player)
            elif self.type == "./Sprites/ennemy/jeanpascaldelaflibustiere/phase6/idle/idle.png":
                self.methods("jp6", closest_player)
            elif self.type == "./Sprites/ennemy/jeanpascaldelaflibustiere/phase7/idle/idle.png":
                self.methods("jp7", closest_player)
            elif self.type == "./Sprites/ennemy/jeanpascaldelaflibustiere/phase8/idle/idle.png":
                self.methods("jp8", closest_player)
            elif self.type == "./Sprites/ennemy/jeanpascaldelaflibustiere/phase9/idle/idle.png":
                self.methods("jp9", closest_player)
            elif self.type == "./Sprites/ennemy/items/key.png":
                self.methods("key", closest_player)
            elif self.type == "./Sprites/ennemy/items/marshmellow.png":
                self.methods("marshmellow", closest_player)
            elif self.type == "./Sprites/ennemy/items/coins.png":
                self.methods("coins", closest_player)
            if self.velx == 0:
                self.animactual = 0
            self.gothrough = False
            self.HPManager("check")
            if self.gotten == True:
                self.img = pygame.image.load("./Sprites/empty.png").convert_alpha()
                self.kill = 0
                self.pv = 0
            if self.invisframe > 0:
                self.invisframe -= 2
            try:
                self.maxlenanim = len(self.anim)
                if self.animactual >= self.maxlenanim - 1:
                    self.animactual = 0
                    self.img = pygame.image.load(self.anim[0]).convert_alpha()
                else:
                    self.animactual += 1
                    self.img = pygame.image.load(self.anim[self.animactual]).convert_alpha()
            except:
                self.img = pygame.image.load(self.type).convert_alpha()
            if self.velx > 0:
                self.direction = "right"
            elif self.velx < 0:
                self.direction = "left"
            self.ix = self.x // self.szbw
            self.iy = self.y // self.szbw
            self.cx, self.cy = self.ix * self.szbw, self.iy * self.szbw
            self.rect = pygame.Rect(self.cx, self.cy, self.szbw, self.szbw)
            if self.isfalling == True:
                self.kill -= 2
            else:
                self.kill = 600
            self.HPManager("check")
        except:
            pass
    def gravity_check(self, rect_list, list_bloc, blocks, blocks_cords_only):
        """vérifie si l'ennemi respecte les lois de la gravité"""
        for elt in rect_list:
            self.temp_rect = pygame.Rect(self.cx, self.cy + 2 * self.szbw, self.szbw, self.szbw)
            if self.temp_rect.colliderect(elt):
                self.vely = 0
                self.isfalling = False
                for eltt in list_bloc:
                    if eltt.rect.x == elt.x and eltt.rect.y == elt.y:
                        eltt.bloc_type_check(self, rect_list, blocks, blocks_cords_only)
                return False
            else:
                self.vely = 7
                self.isfalling = True
        return True
    def wall_check(self, rect_list, list_bloc, blocks, blocks_cords_only):
        """vérifie si l'ennemi peut ou non passer à travers des murs"""
        if self.gothrough == True:
            return False
        if self.rect in rect_list:
            return False
        for elt in rect_list:
            self.temp_rect = pygame.Rect(self.cx + 1, self.cy, self.szbw, self.szbw)
            if self.temp_rect.colliderect(elt) and self.velx < 0:
                for eltt in list_bloc:
                    if eltt.rect.x == elt.x and eltt.rect.y == elt.y:
                        eltt.bloc_type_check(self, rect_list, blocks, blocks_cords_only)
                self.velx = 0
                if self.vely != 0:
                    if self.vely > 0:
                        self.isfalling = True
                    self.vely = -1
                return 'r'
            self.temp_rect = pygame.Rect(self.cx - 1, self.cy, self.szbw, self.szbw)
            if self.temp_rect.colliderect(elt) and self.velx > 0:
                for eltt in list_bloc:
                    if eltt.rect.x == elt.x and eltt.rect.y == elt.y:
                        eltt.bloc_type_check(self, rect_list, blocks, blocks_cords_only)
                self.velx = 10
                if self.vely != 0:
                    if self.vely > 0:
                        self.isfalling = True
                    self.vely = -1
                return 'l'
            if self.temp_rect.colliderect(elt) and self.velx == 0:
                for eltt in list_bloc:
                    if eltt.rect.x == elt.x and eltt.rect.y == elt.y:
                        eltt.bloc_type_check(self, rect_list, blocks, blocks_cords_only)
                if self.direction == "left":
                    self.velx = 1
                    if self.vely != 0:
                        if self.vely > 0:
                            self.isfalling = True
                        self.vely = -1
                    return 'r'
                else:
                    self.velx = 0
                    if self.vely != 0:
                        if self.vely > 0:
                            self.isfalling = True
                        self.vely = -1
                    return 'l'
        return True
    def animation(self, anim):
        """anime les ennemis"""
        if "fly" in self.type:
            try:
                return self.ObtainSprites("/Sprites/ennemy/fly/", anim)
            except:
                return [self.type]
        if "turret" in self.type:
            try:
                return self.ObtainSprites("/Sprites/ennemy/turret/", anim)
            except:
                return [self.type]
        if "penguini" in self.type:
            try:
                return self.ObtainSprites("/Sprites/ennemy/penguini/", anim)
            except:
                return [self.type]
        if "cactus" in self.type:
            try:
                return self.ObtainSprites("/Sprites/ennemy/cactus/", anim)
            except:
                return [self.type]
        if "jeanpascaldelaflibustiere" in self.type:
            if "phase1" in self.type:
                try:
                    return self.ObtainSprites("/Sprites/ennemy/jeanpascaldelaflibustiere/phase1/", anim)
                except:
                    return [self.type]
            elif "phase2" in self.type:
                try:
                    return self.ObtainSprites("/Sprites/ennemy/jeanpascaldelaflibustiere/phase2/", anim)
                except:
                    return [self.type]
            elif "phase3" in self.type:
                try:
                    return self.ObtainSprites("/Sprites/ennemy/jeanpascaldelaflibustiere/phase3/", anim)
                except:
                    return [self.type]
            elif "phase4" in self.type:
                try:
                    return self.ObtainSprites("/Sprites/ennemy/jeanpascaldelaflibustiere/phase4/", anim)
                except:
                    return [self.type]
            elif "phase5" in self.type:
                try:
                    return self.ObtainSprites("/Sprites/ennemy/jeanpascaldelaflibustiere/phase5/", anim)
                except:
                    return [self.type]
            elif "phase6" in self.type:
                try:
                    return self.ObtainSprites("/Sprites/ennemy/jeanpascaldelaflibustiere/phase6/", anim)
                except:
                    return [self.type]
            elif "phase7" in self.type:
                try:
                    return self.ObtainSprites("/Sprites/ennemy/jeanpascaldelaflibustiere/phase7/", anim)
                except:
                    return [self.type]
            elif "phase8" in self.type:
                try:
                    return self.ObtainSprites("/Sprites/ennemy/jeanpascaldelaflibustiere/phase8/", anim)
                except:
                    return [self.type]
            elif "phase9" in self.type:
                try:
                    return self.ObtainSprites("/Sprites/ennemy/jeanpascaldelaflibustiere/phase9/", anim)
                except:
                    return [self.type]
    def ObtainSprites(self, directory, anim):
        """obtient les images des ennemis"""
        l = []
        path = ''
        for letters in directory:
            if letters == '/':
                path += '\\'
            elif letters != '.':
                path += letters
        for elt in os.listdir(os.getcwd()+path+anim+'\\'):
            if elt.endswith(".png"):
                l.append('.'+directory+anim+'/'+elt)
        return l
    def HPManager(self, method, value=0):
        """gère la vie des ennemis"""
        if "jeanpascaldelaflibustiere" not in self.type:
            if self.invisframe == 0:
                value = int(round(value))
                if method == "heal" and value >= 0 and value <= 3 - self.pv:
                    self.pv += value
                if method == "damage" and value >= 0 and value <= self.pv:
                    self.pv -= value
                    self.change_hue()
                    self.invisframe = 200
                if method == "kill":
                    self.pv = 0
                if method == "full":
                    self.pv = 3
                if method == "check":
                    if self.pv <= 0:
                        #self.animation("death")
                        self.kill = 0
                        del self
                return self.pv
        else:
            if "phase1" in self.type and (method == "damage" or method == "kill"):
                self.type = "./Sprites/ennemy/jeanpascaldelaflibustiere/phase2/idle/idle.png"
            elif "phase2" in self.type and (method == "damage" or method == "kill"):
                self.type = "./Sprites/ennemy/jeanpascaldelaflibustiere/phase3/idle/idle.png"
            elif "phase3" in self.type and (method == "damage" or method == "kill"):
                self.type = "./Sprites/ennemy/jeanpascaldelaflibustiere/phase4/idle/idle.png"
            elif "phase4" in self.type and (method == "damage" or method == "kill"):
                self.type = "./Sprites/ennemy/jeanpascaldelaflibustiere/phase5/idle/idle.png"
            elif "phase5" in self.type and (method == "damage" or method == "kill"):
                self.type = "./Sprites/ennemy/jeanpascaldelaflibustiere/phase6/idle/idle.png"
            elif "phase6" in self.type and (method == "damage" or method == "kill"):
                self.type = "./Sprites/ennemy/jeanpascaldelaflibustiere/phase7/idle/idle.png"
            elif "phase7" in self.type and (method == "damage" or method == "kill"):
                self.type = "./Sprites/ennemy/jeanpascaldelaflibustiere/phase8/idle/idle.png"
            elif "phase8" in self.type and (method == "damage" or method == "kill"):
                self.type = "./Sprites/ennemy/jeanpascaldelaflibustiere/phase9/idle/idle.png"
            elif "phase9" in self.type and (method == "damage" or method == "kill"):
                self.type = "./Sprites/ennemy/jeanpascaldelaflibustiere/dead.png"
    def change_hue(self):
        """change la couleur des ennemis"""
        self.color = pygame.Color(0)
        self.color.hsla = (0,100,50,0)
        new_img = pygame.Surface(self.img.get_size())
        new_img.fill(self.color)
        self.img.blit(new_img, (0, 0), special_flags = pygame.BLEND_MULT)
    def methods(self, method, closest_player):
        """dis au ennemis quoi faire"""
        if method == "fly":
            domove = random.randint(0, 16)
            if domove == 5:
                self.velx = 10 * random.choice([-1, 1])
                self.anim = self.animation("walk")
            if abs(self.x - (closest_player.x + 8 * self.szbw)) < 43 and abs(self.y - (closest_player.y + 8 * self.szbw)) < 43:
                closest_player.HPManager("damage", 1)
        elif method == "cactus":
            domove = random.randint(0, 32)
            if domove == 5 :
                self.velx = 10 * random.choice([-1, 1])
                self.vely = -10
                self.anim = self.animation("jump")
            if abs(self.x - (closest_player.x + 8 * self.szbw)) < 43 and abs(self.y - (closest_player.y + 8 * self.szbw)) < 43:
                closest_player.HPManager("damage", 1)
        elif method == "angel":
            distance = self.x - (closest_player.x + 8 * self.szbw) + self.y - (closest_player.y + 8 * self.szbw)
            if distance <= 350 and distance >= -350:
                if distance > 0:
                    if closest_player.direction == "left":
                        self.velx = -2
                    else:
                        self.velx = 0
                if distance < 0:
                    if closest_player.direction == "right":
                        self.velx = 2
                    else:
                        self.velx = 0
                if abs(self.x - (closest_player.x + 8 * self.szbw)) < 43 and abs(self.y - (closest_player.y + 8 * self.szbw)) < 43:
                    closest_player.HPManager("damage", 1)
            else:
                self.velx = 0
        elif "jp" in method:
            if "1" in method:
                distance = self.x - (closest_player.x + 8 * self.szbw) + self.y - (closest_player.y + 8 * self.szbw)
                if distance <= 350 and distance >= -350:
                    if distance > 0:
                        if closest_player.direction == "left":
                            self.velx = -2
                        else:
                            self.velx = 0
                    if distance < 0:
                        if closest_player.direction == "right":
                            self.velx = 2
                        else:
                            self.velx = 0
                    if abs(self.x - (closest_player.x + 8 * self.szbw)) < 60 and abs(self.y - (closest_player.y + 8 * self.szbw)) < 60:
                        closest_player.HPManager("damage", 1)
                else:
                    self.velx = 0
                if self.timer == 0:
                    if len(self.projectiles) < 3:
                        distance_x = self.x - (closest_player.x + 8 * self.szbw)
                        distance_y = self.y - (closest_player.y + 8 * self.szbw)
                        if distance_y < 0:
                            if distance_x < 0:
                                self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 2, "./Sprites/projectiles/laser1.png"))
                            elif distance_x > 0:
                                self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 2, "./Sprites/projectiles/laser1.png"))
                            else:
                                self.projectiles.append(Projectile(self.x, self.y, self.szbw, 0, 2, "./Sprites/projectiles/laser1.png"))
                        elif distance_y > 0:
                            if distance_x < 0:
                                self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, -2, "./Sprites/projectiles/laser1.png"))
                            elif distance_x > 0:
                                self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, -2, "./Sprites/projectiles/laser1.png"))
                            else:
                                self.projectiles.append(Projectile(self.x, self.y, self.szbw, 0, -2, "./Sprites/projectiles/laser1.png"))
                        else:
                            if distance_x < 0:
                                self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/laser1.png"))
                            elif distance_x > 0:
                                self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/laser1.png"))                
                        self.timer = 120
                else:
                    self.timer -= 1
            elif "2" in method:
                domove = random.randint(0, 32)
                if domove == 5 :
                    self.velx = 10 * random.choice([-1, 1])
                    self.vely = -10
                    self.anim = self.animation("jump")
                if abs(self.x - (closest_player.x + 8 * self.szbw)) < 43 and abs(self.y - (closest_player.y + 8 * self.szbw)) < 43:
                    closest_player.HPManager("damage", 1)
                if domove >= 25:
                    if self.timer == 0:
                        if len(self.projectiles) < 5:
                            distance_x = self.x - (closest_player.x + 8 * self.szbw)
                            distance_y = self.y - (closest_player.y + 8 * self.szbw)
                            if distance_y < 0:
                                if distance_x < 0:
                                    self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 2, "./Sprites/projectiles/laser2.png"))
                                elif distance_x > 0:
                                    self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 2, "./Sprites/projectiles/laser2.png"))
                                else:
                                    self.projectiles.append(Projectile(self.x, self.y, self.szbw, 0, 2, "./Sprites/projectiles/laser2.png"))
                            elif distance_y > 0:
                                if distance_x < 0:
                                    self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, -2, "./Sprites/projectiles/laser2.png"))
                                elif distance_x > 0:
                                    self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, -2, "./Sprites/projectiles/laser2.png"))
                                else:
                                    self.projectiles.append(Projectile(self.x, self.y, self.szbw, 0, -2, "./Sprites/projectiles/laser2.png"))
                            else:
                                if distance_x < 0:
                                    self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/laser2.png"))
                                elif distance_x > 0:
                                    self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/laser2.png"))                
                            self.timer = 10
                    else:
                        self.timer -= 1
            elif "3" in method:
                if len(self.projectiles) < 9:
                    distance_x = self.x - (closest_player.x + 8 * self.szbw)
                    distance_y = self.y - (closest_player.y + 8 * self.szbw)
                    if distance_x < 0:
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -1, 0, "./Sprites/projectiles/orb1.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -1, 0, "./Sprites/projectiles/laser4.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -1, 0, "./Sprites/projectiles/orb2.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -1, 0, "./Sprites/projectiles/laser3.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -1, 0, "./Sprites/projectiles/orb3.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -1, 0, "./Sprites/projectiles/laser2.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -1, 0, "./Sprites/projectiles/orb4.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -1, 0, "./Sprites/projectiles/laser1.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -1, 0, "./Sprites/projectiles/orb5.png"))
                    elif distance_x > 0:
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 1, 0, "./Sprites/projectiles/orb1.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 1, 0, "./Sprites/projectiles/laser4.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 1, 0, "./Sprites/projectiles/orb2.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 1, 0, "./Sprites/projectiles/laser3.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 1, 0, "./Sprites/projectiles/orb3.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 1, 0, "./Sprites/projectiles/laser2.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 1, 0, "./Sprites/projectiles/orb4.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 1, 0, "./Sprites/projectiles/laser1.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 1, 0, "./Sprites/projectiles/orb5.png"))
            elif "4" in method:
                domove = random.randint(0, 16)
                if domove == 5:
                    self.velx = 10 * random.choice([-1, 1])
                    self.anim = self.animation("walk")
                if abs(self.x - (closest_player.x + 8 * self.szbw)) < 43 and abs(self.y - (closest_player.y + 8 * self.szbw)) < 43:
                    closest_player.HPManager("damage", 1) 
            elif "5" in method:
                if len(self.projectiles) < 9:
                    if self.timer == 0:
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-1.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-2.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-3.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-4.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-5.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-6.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-7.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-8.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-9.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-10.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-11.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-12.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-13.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-14.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-15.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-16.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-17.png"))
                        self.projectiles.append(Projectile(closest_player.x, closest_player.y - self.szbw, self.szbw, 0, 1, "./Sprites/projectiles/laser2-18.png"))
                        self.timer = 210
                    else:
                        self.timer -= 1
            elif "6" in method:
                if len(self.projectiles) < 9:
                    distance_x = self.x - (closest_player.x + 8 * self.szbw)
                    distance_y = self.y - (closest_player.y + 8 * self.szbw)
                    if distance_x < 0:
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/orb1.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/laser4.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/orb2.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/laser3.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/orb3.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/laser2.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/orb4.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/laser1.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/orb5.png"))
                    elif distance_x > 0:
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/orb1.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/laser4.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/orb2.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/laser3.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/orb3.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/laser2.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/orb4.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/laser1.png"))
                        self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/orb5.png"))
            elif "7" in method:
                domove = random.randint(0, 16)
                if domove == 5:
                    self.anim = self.animation("attack")
                    distance_x = self.x - (closest_player.x + 8 * self.szbw)
                    distance_y = self.y - (closest_player.y + 8 * self.szbw)
                    if abs(distance_x) <= 70 and abs(distance_y <= 70):
                        closest_player.HPManager("damage", 1)
            elif "8" in method:
                self.anim = self.animation("attack")
                domove = random.choice([
                self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 2, "./Sprites/projectiles/projectile_fireball.png")),
                self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 2, "./Sprites/projectiles/projectile_fireball.png")),
                self.projectiles.append(Projectile(self.x, self.y, self.szbw, 0, 2, "./Sprites/projectiles/projectile_fireball.png")),
                self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, -2, "./Sprites/projectiles/projectile_fireball.png")),
                self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, -2, "./Sprites/projectiles/projectile_fireball.png")),
                self.projectiles.append(Projectile(self.x, self.y, self.szbw, 0, -2, "./Sprites/projectiles/projectile_fireball.png")),
                self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/projectile_fireball.png")),
                self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/projectile_fireball.png"))
                ])
        elif method == "turret":
            if self.timer == 0:
                if len(self.projectiles) < 3:
                    distance_x = self.x - (closest_player.x + 8 * self.szbw)
                    distance_y = self.y - (closest_player.y + 8 * self.szbw)
                    if distance_y < 0:
                        if distance_x < 0:
                            self.anim = self.animation("attack_left")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 2, "./Sprites/projectiles/turret.png"))
                        elif distance_x > 0:
                            self.anim = self.animation("attack_right")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 2, "./Sprites/projectiles/turret.png"))
                        else:
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, 0, 2, "./Sprites/projectiles/turret.png"))
                    elif distance_y > 0:
                        if distance_x < 0:
                            self.anim = self.animation("attack_left")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, -2, "./Sprites/projectiles/turret.png"))
                        elif distance_x > 0:
                            self.anim = self.animation("attack_right")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, -2, "./Sprites/projectiles/turret.png"))
                        else:
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, 0, -2, "./Sprites/projectiles/turret.png"))
                    else:
                        if distance_x < 0:
                            self.anim = self.animation("attack_left")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/turret.png"))
                        elif distance_x > 0:
                            self.anim = self.animation("attack_right")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/turret.png"))                
                    self.timer = 120
            else:
                self.timer -= 1
        elif method == "penguini":
            domove = random.randint(0, 16)
            if domove == 5:
                self.velx = 10 * random.choice([-1, 1])
                self.anim = self.animation("walk")
            if abs(self.x - (closest_player.x + 8 * self.szbw)) < 43 and abs(self.y - (closest_player.y + 8 * self.szbw)) < 43:
                closest_player.HPManager("damage", 1)
            if self.timer == 0:
                if len(self.projectiles) < 3:
                    distance_x = self.x - (closest_player.x + 8 * self.szbw)
                    distance_y = self.y - (closest_player.y + 8 * self.szbw)
                    if distance_y < 0:
                        if distance_x < 0:
                            self.anim = self.animation("attack")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 2, "./Sprites/projectiles/ice.png"))
                        elif distance_x > 0:
                            self.anim = self.animation("attack")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 2, "./Sprites/projectiles/ice.png"))
                        else:
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, 0, 2, "./Sprites/projectiles/ice.png"))
                    elif distance_y > 0:
                        if distance_x < 0:
                            self.anim = self.animation("attack")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, -2, "./Sprites/projectiles/ice.png"))
                        elif distance_x > 0:
                            self.anim = self.animation("attack")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, -2, "./Sprites/projectiles/ice.png"))
                        else:
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, 0, -2, "./Sprites/projectiles/ice.png"))
                    else:
                        if distance_x < 0:
                            self.anim = self.animation("attack")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, 2, 0, "./Sprites/projectiles/ice.png"))
                        elif distance_x > 0:
                            self.anim = self.animation("attack")
                            self.projectiles.append(Projectile(self.x, self.y, self.szbw, -2, 0, "./Sprites/projectiles/ice.png"))                
                    self.timer = 120
            else:
                self.timer -= 1
        elif method == "key":
            if self.gotten == False and abs(self.x - (closest_player.x + 8 * self.szbw)) <= 18 and abs(self.y - (closest_player.y + 8 * self.szbw)) <= 44:
                self.gotten = True
                closest_player.items["key"] += 1
        elif method == "marshmellow":
            if self.gotten == False and abs(self.x - (closest_player.x + 8 * self.szbw)) <= 18 and abs(self.y - (closest_player.y + 8 * self.szbw)) <= 44:
                self.gotten = True
                closest_player.items["power"] = 1
        elif method == "coins":
            if self.gotten == False and abs(self.x - (closest_player.x + 8 * self.szbw)) <= 48 and abs(self.y - (closest_player.y + 8 * self.szbw)) <= 48:
                self.gotten = True
                closest_player.items["coins"] += 1
        if method not in ["coins","key","marshmellow"]:
            to_des = []
            for elt in closest_player.projectiles:
                if abs(self.x - elt.x) < 48 and abs(self.y - elt.y) < 48:
                    self.HPManager("damage", 1)
                    to_des.append(elt)
            for eltt in to_des:
                closest_player.projectiles.remove(eltt)
            if closest_player.items["power"] == 5:
                if abs(self.x - (closest_player.x + 8 * self.szbw)) <= 48 and abs(self.y - (closest_player.y + 8 * self.szbw)) <= 48:
                    self.HPManager("kill")
                
            
class Projectile(pygame.sprite.Sprite):
    """gère les projectiles"""
    def __init__(self, x, y, szbw, velx, vely, projectile_type):
        pygame.sprite.Sprite.__init__(self)
        self.velx = velx
        self.img = pygame.image.load(projectile_type).convert_alpha()
        self.vely = vely
        self.x = x
        self.y = y
        self.szbw = szbw
        self.ix = self.x // self.szbw
        self.iy = self.y // self.szbw
        self.cx, self.cy = self.ix * self.szbw, self.iy * self.szbw
        self.rect = pygame.Rect(self.cx, self.cy, 42, 42)
        self.kill == False
        self.gothrough = False
    def gravity_check(self, rect_list):
        for elt in rect_list:
            self.temp_rect = pygame.Rect(self.cx, self.cy + 2 * self.szbw, 42, 42)
            if self.temp_rect.colliderect(elt):
                self.isfalling = False
                return True
            else:
                return False
        return False
    def wall_check(self, rect_list):
        if self.gothrough == True:
            return False
        if self.rect in rect_list:
            return False
        for elt in rect_list:
            self.temp_rect = pygame.Rect(self.cx, self.cy, 42, 42)
            if self.temp_rect.colliderect(elt):
                return True
        return False
    def update_walk_die(self, target, surface, dx, dy, rect_list):
        self.x += self.velx
        self.y += self.vely
        self.dx = self.x - dx
        self.dy = self.y - dy
        if self.dx > 0 and self.dy > 0:
            surface.blit(self.img, (self.dx,self.dy))
        if self.wall_check(rect_list) == True or self.gravity_check(rect_list) == True:
            self.kill = True
        self.distance_x = abs(self.x - (target.x + 8 * self.szbw))
        self.distance_y = abs(self.y - (target.y + 8 * self.szbw))
        if round(self.distance_x) <= 42 and round(self.distance_y) <= 42:
            target.HPManager("damage",1)
            self.kill = True
        elif round(self.distance_x) > (693 // 2) or round(self.distance_y) > (693 // 2):
            self.kill = True
    def update_for_player(self, surface, rect_list, camera):
        self.x += self.velx
        self.y += self.vely
        self.dx = self.x - camera.offset.x
        self.dy = self.y - camera.offset.y
        surface.blit(self.img, (self.dx,self.dy))
        if self.wall_check(rect_list) == True or self.gravity_check(rect_list) == True:
            self.kill = True


class Player(pygame.sprite.Sprite):
    """gère le joueur"""
    def __init__(self, x, y, szbw):
        pygame.sprite.Sprite.__init__(self)
        self.animactual = 0
        self.cooldown = 0
        self.kill = 600
        self.x = x
        self.y = y
        self.szbw = szbw
        self.ix = self.x // self.szbw
        self.iy = self.y // self.szbw
        self.cx, self.cy = self.ix * self.szbw, self.iy * self.szbw
        self.velx = 0
        self.vely = 0 
        self.isfalling = False
        self.to_del = []
        self.img = pygame.image.load("./Sprites/player/1/idle/idle.png").convert_alpha()
        self.rect = pygame.Rect(self.cx, self.cy, self.szbw, self.szbw)
        self.anim = self.animation("idle")
        self.direction = "left"
        self.items = {"key": 0, "power": 0, "coins": 0}
        self.oldx = self.x
        self.pv = 3
        self.invisframe = 0
        self.gothrough = False
        self.win = False
        self.projectiles = []
    def walk(self, rect_list, list_block_literral, blocks, blocks_cords_only):
        self.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only)
        if (self.x != (self.x + self.velx)):
            self.anim = self.animation("walk")
            if self.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == True:
                self.oldx = self.x
                self.x += self.velx
            elif self.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == 'r' or self.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == 'l':
                self.velx = 0
                if self.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == 'r':
                    self.x = self.oldx + 10
                    self.oldx = self.x
                else:
                    self.x = self.oldx - 10
                    self.oldx = self.x
        if self.gothrough == True:
            self.isfalling = False
            self.y += self.vely
        if self.y != (self.y + self.vely):
            if self.vely > 0:
                #self.anim = self.animation("fall")
                self.y += self.vely
            elif self.gothrough == False and self.vely < 0:
                if self.isfalling == False:
                    #self.anim = self.animation("jump")
                    for elt in rect_list:
                        self.temp_rect = pygame.Rect(self.cx + 8 * self.szbw, self.cy + 8 * self.szbw, self.szbw, self.szbw)
                        if self.temp_rect.colliderect(elt):
                            self.vely = 7
                            self.isfalling = True
                            return -1
                        self.temp_rect = pygame.Rect(self.cx + 8 * self.szbw, self.cy + 9 * self.szbw, self.szbw, self.szbw)
                        if self.temp_rect.colliderect(elt):
                            self.vely = 7
                            self.isfalling = True
                            return -1
                        self.temp_rect = pygame.Rect(self.cx + 8 * self.szbw, self.cy + 7 * self.szbw, self.szbw, self.szbw)
                        if self.temp_rect.colliderect(elt):
                            self.vely = 7
                            self.isfalling = True
                            return -1
                    self.y += self.vely
    def update(self, surface, camera, rect_list):
        for eltt in self.to_del:
            self.projectiles.remove(eltt)
        self.to_del = []
        if self.velx == 0:
            self.anim = self.animation("idle")
        self.gothrough = False
        self.HPManager("check")
        if self.invisframe > 0:
            self.invisframe -= 2
        self.maxlenanim = len(self.anim)
        if self.animactual >= self.maxlenanim - 1:
            self.animactual = 0
            self.img = pygame.image.load(self.anim[0]).convert_alpha()
        else:
            self.animactual += 1
            self.img = pygame.image.load(self.anim[self.animactual]).convert_alpha()
        self.camera = camera
        self.ix = self.x // self.szbw
        self.iy = self.y // self.szbw
        self.cx, self.cy = self.ix * self.szbw, self.iy * self.szbw
        self.rect = pygame.Rect(self.x + 342, self.y + 336, 34, 96)
        if self.isfalling == True:
            self.kill -= 2
        else:
            self.kill = 600
        self.HPManager("check")
    def gravity_check(self, rect_list, list_bloc, blocks, blocks_cords_only):
        for elt in rect_list:
            self.temp_rect = pygame.Rect(self.cx + 8 * self.szbw, self.cy + 10 * self.szbw, self.szbw, self.szbw)
            if self.temp_rect.colliderect(elt):
                self.vely = 0
                self.isfalling = False
                for eltt in list_bloc:
                    if eltt.rect.x == elt.x and eltt.rect.y == elt.y:
                        eltt.bloc_type_check(self, rect_list, blocks, blocks_cords_only)
                return False
            else:
                self.vely = 7
                self.isfalling = True
        return True
    def wall_check(self, rect_list, list_bloc, blocks, blocks_cords_only):
        if self.gothrough == True:
            return False
        if self.rect in rect_list:
            return False
        for elt in rect_list:
            self.temp_rect = pygame.Rect(self.cx + 10 + 8 * self.szbw, self.cy + 9 * self.szbw, self.szbw - 10, self.szbw)
            if self.temp_rect.colliderect(elt) and self.velx < 0:
                for eltt in list_bloc:
                    if eltt.rect.x == elt.x and eltt.rect.y == elt.y:
                        eltt.bloc_type_check(self, rect_list, blocks, blocks_cords_only)
                self.velx = 0
                if self.vely != 0:
                    if self.vely > 0:
                        self.isfalling = True
                    self.vely = -1
                return 'r'
            self.temp_rect = pygame.Rect(self.cx + 8 * self.szbw, self.cy + 9 * self.szbw, self.szbw, self.szbw)
            if self.temp_rect.colliderect(elt) and self.velx > 0:
                for eltt in list_bloc:
                    if eltt.rect.x == elt.x and eltt.rect.y == elt.y:
                        eltt.bloc_type_check(self, rect_list, blocks, blocks_cords_only)
                self.velx = 10
                if self.vely != 0:
                    if self.vely > 0:
                        self.isfalling = True
                    self.vely = -1
                return 'l'
            if self.temp_rect.colliderect(elt) and self.velx == 0:
                for eltt in list_bloc:
                    if eltt.rect.x == elt.x and eltt.rect.y == elt.y:
                        eltt.bloc_type_check(self, rect_list, blocks, blocks_cords_only)
                if self.direction == "left":
                    self.velx = 1
                    if self.vely != 0:
                        if self.vely > 0:
                            self.isfalling = True
                        self.vely = -1
                    return 'r'
                else:
                    self.velx = 0
                    if self.vely != 0:
                        if self.vely > 0:
                            self.isfalling = True
                        self.vely = -1
                    return 'l'
        return True
    def animation(self, anim):
        try:
            a = self.ObtainSprites("/Sprites/player/1/", anim)
            a = sorted(a)
            return a
        except:
            return ["./Sprites/player/1/idle/idle.png", "./Sprites/player/idle/idle.png", "./Sprites/player/idle/idle.png", "./Sprites/player/idle/idle.png", "./Sprites/player/idle/idle.png", "./Sprites/player/idle/idle.png", "./Sprites/player/idle/idle2.png", "./Sprites/player/idle/idle2.png", "./Sprites/player/idle/idle2.png", "./Sprites/player/idle/idle2.png", "./Sprites/player/idle/idle2.png", "./Sprites/player/idle/idle2.png", "./Sprites/player/idle/idle3.png", "./Sprites/player/idle/idle3.png", "./Sprites/player/idle/idle3.png", "./Sprites/player/idle/idle3.png", "./Sprites/player/idle/idle3.png", "./Sprites/player/idle/idle4.png", "./Sprites/player/idle/idle4.png", "./Sprites/player/idle/idle4.png", "./Sprites/player/idle/idle4.png", "./Sprites/player/idle/idle4.png", "./Sprites/player/idle/idle4.png", "./Sprites/player/idle/idle5.png", "./Sprites/player/idle/idle5.png", "./Sprites/player/idle/idle5.png", "./Sprites/player/idle/idle5.png", "./Sprites/player/idle/idle5.png", "./Sprites/player/idle/idle5.png"]
    def ObtainSprites(self, directory, anim):
        l = []
        path = ''
        for letters in directory:
            if letters == '/':
                path += '\\'
            elif letters != '.':
                path += letters
        for elt in os.listdir(os.getcwd()+path+anim+'\\'):
            if elt.endswith(".png"):
                l.append('.'+directory+anim+'/'+elt)
        if anim == "walk":
            return l * ((int(19 / abs(self.velx))) + 1)
        else:
            return l
    def Capacity(self, rect_list, screen):
        """gère les capacités du joueur"""
        selected = ""
        if self.items["power"] == 1:
            selected = "marshmellow"
        if self.items["power"] == 5:
            selected = "invicibility"
        if selected == "marshmellow":
            if len(self.projectiles) <= 20:
                if self.direction == "left":
                    self.projectiles.append(Projectile(self.x + 8 * self.szbw, self.y + 8 * self.szbw, self.szbw, -2, 0, "./Sprites/projectiles/projectile_fireball.png"))
                else:
                    self.projectiles.append(Projectile(self.x + 8 * self.szbw, self.y + 8 * self.szbw, self.szbw, 2, 0, "./Sprites/projectiles/projectile_fireball.png"))
            else:
                self.projectiles.pop(0)
        if selected == "invicibility":
            self.color = pygame.Color(0)
            self.color.hsla = (46,65,52,50)
            new_img = pygame.Surface(self.img.get_size())
            new_img.fill(self.color)
            self.img.blit(new_img, (0, 0), special_flags = pygame.BLEND_MULT)
    def HPManager(self, method, value=0):
        if self.invisframe == 0:
            value = int(round(value))
            if method == "heal" and value >= 0 and value <= 3 - self.pv:
                self.pv += value
            if method == "damage" and value >= 0 and value <= self.pv:
                if self.items["power"] == 1:
                    self.pv -= value
                    self.change_hue()
                    self.invisframe = 200
                elif self.items["power"] <= 4 and self.items["power"] > 0:
                    self.items["power"] -= 1
                    self.projectiles = []
                elif self.items["power"] == 5:
                    pass
            if method == "kill":
                self.pv = 0
            if method == "full":
                self.pv = 3
            if method == "check":
                if self.pv <= 0:
                    #self.animation("death")
                    self.kill = 0
            return self.pv
    def change_hue(self):
        self.color = pygame.Color(0)
        self.color.hsla = (0,100,50,0)
        new_img = pygame.Surface(self.img.get_size())
        new_img.fill(self.color)
        self.img.blit(new_img, (0, 0), special_flags = pygame.BLEND_MULT)


def quitting_to_main_menu(player, level_completed, lives, world, tile):
    """quitte vers le menu principal et sauvegarde la progression"""
    filenameS = "./world_savedata.txt"
    if filenameS != "":
        TEMP_Save = ""
        Saves = open(filenameS,"w+")
        Saves.truncate()
        TEMP_Save = '(' + str(tile) + ',' + str(world) + ',' + str(player.items['coins']) + ',' + str(lives) + ',' + str(level_completed) + ',' + str(player.items['power']) + ')'
        Saves.writelines(TEMP_Save)
        Saves.close()
    exit()


def memorize(func):
    """fonction cache qui sert de décorateur"""
    cache = {}
    @wraps(func)
    def wrapper(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key not in cache:
            cache[key] = func(*args, **kwargs)
        return cache[key]
    return wrapper


@memorize
def Load(player, blocks, list_block_literral, screen, blocks_cords_only):
    """Permet de charger le niveau"""
    E = ()
    blocks2 = []
    levels_completed, lives, world, tile = 0,5,0,0
    with open("./world_savedata.txt","r") as SelectLevels:
        for lines in SelectLevels.readlines():
            E = parse_tuple(lines)
    tile = E[0]
    world = E[1]
    player.items['coins'] = E[2]
    lives = E[3]
    level_completed = E[4]
    player.items['power'] = E[5]
    SelectLevels.close()
    E = []
    filenameL = "./World/" + str(world) + "/" + str(tile) + ".txt"
    if filenameL != "":
        try:
            with open(str(filenameL),"r") as Load_Save:   
                for line in Load_Save.readlines():
                    E.append(line)
            clold = E[-4]
            cl = (parse_tuple(clold))
            spawnpoint = parse_tuple(E[-5])
            sp = (int(spawnpoint[0]) - 8 * screen.get_width() // 16, int(spawnpoint[1]) - 0.75 * screen.get_width())
            ennemies_list = E[-1]
            ennemies_list = ennemies_list[1:-2]
            cached = ""
            flag = False
            enn = []
            enn3 = []
            for elt in ennemies_list :
                if elt == "," or elt == " ":
                    if flag == False:
                        cached += elt
                elif elt != "'":
                    if elt == '(' and flag == True:
                        flag = False
                    if elt != ')':
                        cached += elt
                    else:
                        cached += elt
                        flag = True
                        enn.append(parse_tuple(cached))
                        cached = ''
            removing = []
            for enn2 in enn:
                if enn2 == None:
                    removing.append(enn2)
            for rem in removing:
                enn.remove(rem)
            for ennb in enn:
                enn3.append(Ennemy(ennb[0], ennb[1], screen.get_width() // 16, ennb[2]))
            for eltt in E:
                if eltt != clold and eltt != E[-3] and eltt != E[-2] and eltt != E[-1]:
                    Tpl = (parse_tuple(eltt))
                    sz = screen.get_width() // 16
                    cx = Tpl[0]
                    cy = Tpl[1]
                    typebloc = Tpl[2]
                    if os.path.exists(typebloc) != True:
                        raise Exception
                blocks2.append(Tpl)
            Load_Save.close()
            return cl, blocks2, enn3, sp, levels_completed, lives, world, tile
        except:
            sg.popup('Sauvegarde vide ou corrompue.', title='Erreur', button_color=('#A81B0C', '#FFFFFF'), background_color='#F47264', line_width=29, custom_text='Ok')
            quitting_to_main_menu(player, levels_completed, lives, world, tile)
    else:
         sg.popup('Aucune sauvegarde entrée.', title='Erreur', button_color=('#A81B0C', '#FFFFFF'), background_color='#F47264', line_width=25, custom_text='Ok')
         quitting_to_main_menu(player, levels_completed, lives, world, tile)
         
        
@memorize    
def parse_tuple(string):
    """transforme une string en tuple"""
    try:
        s = ast.literal_eval(str(string))
        if type(s) == tuple:
            return s
    except:
        return
    
    
def draw_load(surface, sizebtwn, type_bloc, blocks, blocks_cords_only, list_block_literral, cx, cy, rect_list, sprites, platforms):
    """charge chaque bloc"""
    bloc_temp = Bloc(blocks, type_bloc, cx, cy, list_block_literral, rect_list, sizebtwn)
    sprites.add(bloc_temp)
    platforms.add(bloc_temp)
    bloc_temp.draw_bloc(surface, blocks_cords_only, rect_list, blocks, 0, 0)
    bloc_temp.kill()
    
        
def drawontop(surface, sizebtwn, blocks, colorbg, list_block_literral, blocks_cords_only, dx, dy):
    """dessine les blocs"""
    for elt in blocks:
        local_cx = 0
        local_cy = 0
        if elt != colorbg:
            local_cx = elt[0] - dx
            local_cy = elt[1] - dy
            local_img = elt[2]
            if local_cx > 0 and local_cy > 0:
                surface.blit(local_img, (local_cx,local_cy))
    for eltt in list_block_literral:
        eltt.update()
        
        
def drawontop_ennemies(surface, sizebtwn, ennemies, dx, dy):
    """dessine les ennemis"""
    for ennemy in ennemies:
        local_cx = ennemy.x - dx
        local_cy = ennemy.y - dy
        local_img = ennemy.img
        if local_cx > 0 and local_cy > 0:
            try:
                surface.blit(local_img, (local_cx,local_cy))
            except:
                continue
        
        
def deathp(sc, player, levels_completed, lives, world, tile):
    """Game Over"""
    pygame.mixer.music.stop()
    lives -= 1
    if lives < 0:
        lives = 5
        tile = 0
    sc.fill((255,0,0))
    pygame.display.flip()
    sg.popup('GAME OVER', title='', button_color=('#A81B0C', '#FFFFFF'), background_color='#F47264', line_width=25, custom_text='Revenir au menu principal')
    quitting_to_main_menu(player, levels_completed, lives, world, tile)
               
               
def win(sc, player, levels_completed, lives, world, tile):
    """Gagne !"""
    tile += 1
    if tile > 1:
        tile = 0
        world += 1
    if world > 4:
        tile = 0
        world = 0
    levels_completed += 1
    sc.fill((0,255,0))
    pygame.display.flip()
    sg.popup('VICTOIRE', title='', button_color=('#45f542', '#FFFFFF'), background_color='#9af299', line_width=25, custom_text='Revenir au menu principal')
    quitting_to_main_menu(player, levels_completed, lives, world, tile)
    
    
def update_mouse_cursor(sizebtwn):
    """Met a jour la position de la souris"""
    x, y = pygame.mouse.get_pos()
    ix = x // sizebtwn
    iy = y // sizebtwn
    cx, cy = ix * sizebtwn, iy * sizebtwn
    return cx, cy
       
       
def main():
    """programme principal"""
    tempload = None
    sys.setswitchinterval(500)
    pygame.init()
    screen = pygame.display.set_mode((693, 693), pygame.DOUBLEBUF, 48)
    clock = pygame.time.Clock()
    fps_font = pygame.font.SysFont((), 64)
    rect_list = []
    List_To_Draw = []
    list_block_literral = []
    blocks = []
    blocks_cords_only = []
    items = []
    insts = None
    game_savemenu = False
    game_itemmenu = False
    game_backgroundmenu = False
    pygame.mixer.pre_init(44100, -16, 2, 2048)
    pygame.mixer.music.load("./Music/Kevin MacLeod - Pixelland.mp3")
    acc = 1
    canvas = pygame.Surface((screen.get_width(),screen.get_height()))
    platforms = pygame.sprite.Group()
    sprites = pygame.sprite.Group()
    bloc_type = "./Sprites/grass.png"
    flaga = True
    run = True
    player = Player(0, 0, screen.get_width() // 16)
    camera = Camera(player, screen)
    follow = Suivre(camera, player)
    auto = Auto(camera, player, acc)
    activemethod = follow
    camera.setmeth(activemethod)
    load1, load2, load3, load4, levels_completed, lives, world, tile = Load(player, blocks, list_block_literral, canvas, blocks_cords_only)
    tempload = (load1, load2, load3, load4)
    if tempload != None:
        colorbg, List_To_Draw, ennemies, spawnpoint = tempload
    else:
        colorbg = None
    player.x, player.y = spawnpoint
    Turn1 = True
    to_rem = []
    while run:
        for ennemy in ennemies:
            if ennemy.pv <= 0 or ennemy.kill <= 0:
                to_rem.append(ennemy)
        for rem in to_rem:
            ennemies.remove(rem)
        to_rem = []
        clock.tick(30)
        if colorbg != None:   
            screen.fill(colorbg)
            drawontop(canvas, screen.get_width() // 16, blocks, colorbg, list_block_literral, blocks_cords_only, camera.offset.x, camera.offset.y)
            drawontop_ennemies(canvas, screen.get_width() // 16, ennemies, camera.offset.x, camera.offset.y)
            player.update(canvas, camera, rect_list)
            for ennemy in ennemies:
                ennemy.update(canvas, player, rect_list, camera.offset.x, camera.offset.y)
            camera.scroll()
        else:
            colorbg = (0,0,0)
            screen.fill(colorbg)
            drawontop(canvas, screen.get_width() // 16, blocks, colorbg, list_block_literral, blocks_cords_only, camera.offset.x, camera.offset.y)
            drawontop_ennemies(canvas, screen.get_width() // 16, ennemies, camera.offset.x, camera.offset.y)
            player.update(canvas, camera, rect_list)
            for ennemy in ennemies:
                ennemy.update(canvas, player, rect_list, camera.offset.x, camera.offset.y)
            camera.scroll()
        if Turn1 == True:
            pygame.mixer.music.play(-1)
            timeleft = 360
            timer_event = pygame.USEREVENT + 1
            pygame.time.set_timer(timer_event , timeleft)
            for elt in List_To_Draw:
                if elt != colorbg:
                    draw_load(canvas, screen.get_width() // 16, elt[2], blocks, blocks_cords_only, list_block_literral, elt[0], elt[1], rect_list, sprites, platforms)
                sprites.add(player)
            Turn1 = False
        if player.pv == 1:
            canvas.blit(pygame.image.load("./Sprites/UI/heart.png"), (100,0))
        if player.pv == 2:
            canvas.blit(pygame.image.load("./Sprites/UI/heart.png"), (100,0))
            canvas.blit(pygame.image.load("./Sprites/UI/heart.png"), (150,0))
        if player.pv == 3:
            canvas.blit(pygame.image.load("./Sprites/UI/heart.png"), (100,0))
            canvas.blit(pygame.image.load("./Sprites/UI/heart.png"), (150,0))
            canvas.blit(pygame.image.load("./Sprites/UI/heart.png"), (200,0))
        for event in pygame.event.get():
            keys = pygame.key.get_pressed()
            if event.type == pygame.QUIT:
                run = False
            if event.type == timer_event:
                if timeleft > 100:
                    timeleft -= 0.75
                elif timeleft <= 100 and timeleft > 0:
                    timeleft -= 0.5
                elif timeleft <= 0:
                    deathp(screen, player, levels_completed, lives, world, tile)
            if event.type == pygame.KEYDOWN and player.pv != 0:
                pygame.key.set_repeat(10)
                if keys[pygame.K_LEFT]:
                    if activemethod != auto:
                        if player.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == True:
                            if abs(player.velx) < 5:
                                player.velx = -5
                                player.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only)
                                player.direction = "left"
                if keys[pygame.K_RIGHT]:
                    if activemethod != auto:
                        if player.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == True:
                            if abs(player.velx) < 5:
                                player.velx = 5
                                player.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only)
                                player.direction = "right"
                    else:
                        if acc <= 5:
                            if player.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only) == True:
                                acc += 1
                                player.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only)
                if keys[pygame.K_UP] and player.velx > 0:
                    pygame.key.set_repeat()
                    if player.isfalling == False and player.cooldown == 0:
                        player.vely = -125
                        player.velx = 10
                        pygame.mixer.Sound.play(pygame.mixer.Sound("./Music/SE/jump.wav"))
                        player.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only)
                        player.cooldown = 200
                elif keys[pygame.K_UP] and player.velx < 0:
                    pygame.key.set_repeat()
                    if player.isfalling == False and player.cooldown == 0:
                        player.vely = -125
                        player.velx = -10
                        pygame.mixer.Sound.play(pygame.mixer.Sound("./Music/SE/jump.wav"))
                        player.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only)
                        player.cooldown = 200
                elif keys[pygame.K_UP] and player.velx == 0:
                    pygame.key.set_repeat()
                    if player.isfalling == False and player.cooldown == 0:
                        player.vely = -200
                        pygame.mixer.Sound.play(pygame.mixer.Sound("./Music/SE/jump.wav"))
                        player.wall_check(rect_list, list_block_literral, blocks, blocks_cords_only)
                        player.cooldown = 200
            click = pygame.mouse.get_pressed()
            if click[0]:
                player.Capacity(rect_list, canvas)
        for elt in player.projectiles:
            if elt.kill == True:
                player.to_del.append(elt)
            else:
                elt.update_for_player(canvas, rect_list, player.camera)
        if player.items["coins"] >= 100:
            coinsmod = player.items["coins"] // 100
            lives += coinsmod
            player.items["coins"] -= 100 * coinsmod
        player.walk(rect_list, list_block_literral, blocks, blocks_cords_only)
        player.gravity_check(rect_list, list_block_literral, blocks, blocks_cords_only)
        if player.win == True:
            win(screen, player, levels_completed, lives, world, tile)
        for ennemy in ennemies:
            ennemy.walk(rect_list, list_block_literral, blocks, blocks_cords_only)
            ennemy.gravity_check(rect_list, list_block_literral, blocks, blocks_cords_only)
        for item in items:
            item.walk(rect_list, list_block_literral, blocks, blocks_cords_only)
            item.gravity_check(rect_list, list_block_literral, blocks, blocks_cords_only)
        if player.direction == "right":
            canvas.blit(pygame.transform.flip(player.img,True,False), (screen.get_width() // 2, screen.get_width() // 2))
        elif player.direction == "left":
            canvas.blit(pygame.transform.flip(player.img,False,False), (screen.get_width() // 2, screen.get_width() // 2))
        screen.blit(canvas, (0, 0))
        fps_surface = fps_font.render(clock.get_fps().__int__().__str__(), False, (100,50,50))
        timer_surface = pygame.font.SysFont(None, 32).render(f"TEMPS : {timeleft}", True, (255,255,255))
        screen.blit(fps_surface, (0,0))
        screen.blit(timer_surface, (screen.get_rect().right - 135,0))
        pygame.display.flip()
        screen.fill(colorbg)
        canvas.fill(colorbg)
        if acc > 1:
            acc -= 1
        if acc < 1:
            acc += 1
        if player.velx != 0:
            if player.velx < 0.2 and player.velx > -0.2:
                player.velx = 0
            if player.velx > 0:
                player.velx -= 0.35
            else:
                player.velx += 0.35
        if player.vely != 0:
            if player.vely < 14:
                player.vely *= 2
            if player.vely < 0:
                player.vely -= 1
        camera.scroll()
        if player.cooldown > 0:
            player.cooldown -= 5
        if player.kill == 0:
            deathp(screen, player, levels_completed, lives, world, tile)
    quitting_to_main_menu(player, levels_completed, lives, world, tile)


main()