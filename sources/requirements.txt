altgraph==0.17.4
astroid==2.4.2
asttokens==2.0.4
auto-py-to-exe==2.43.2
bcrypt==3.2.0
bitstring==3.1.7
bottle==0.12.25
bottle-websocket==0.2.9
certifi==2024.2.2
cffi==1.14.4
charset-normalizer==3.3.2
colorama==0.4.4
cryptography==3.2.1
cycler==0.11.0
docutils==0.16
ecdsa==0.16.1
Eel==0.16.0
esptool==3.0
fonttools==4.37.3
future==1.0.0
gevent==22.10.2
gevent-websocket==0.10.1
greenlet==3.0.3
idna==3.6
importlib-metadata==6.7.0
isort==5.6.4
jedi==0.17.2
keyboard==0.13.5
kiwisolver==1.4.4
lazy-object-proxy==1.4.3
matplotlib==3.5.3
mccabe==0.6.1
mypy==0.790
mypy-extensions==0.4.3
netifaces==0.11.0
numpy==1.21.6
packaging==24.0
paramiko==2.7.2
parso==0.7.1
pefile==2023.2.7
Pillow==9.2.0
PodSixNet==0.11.0
ptyprocess==0.6.0
pycparser==2.20
pygame==2.1.2
pyinstaller==5.13.2
pyinstaller-hooks-contrib==2024.3
pyKey==0.2
pylint==2.6.0
PyNaCl==1.4.0
pyparsing==3.0.9
pyserial==3.5
PySimpleGUI==4.60.4
python-chess==1.999
python-dateutil==2.8.2
pywin32-ctypes==0.2.2
reedsolo==1.5.4
requests==2.31.0
Send2Trash==1.5.0
six==1.15.0
thonny==3.3.1
toml==0.10.2
typed-ast==1.4.1
typing-extensions==3.10.0.0
urllib3==2.0.7
websockets==8.1
whichcraft==0.6.1
wrapt==1.12.1
zipp==3.15.0
zope.event==5.0
zope.interface==6.2
