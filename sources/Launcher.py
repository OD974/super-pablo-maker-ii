"""Le Launcher lance les programmes OnlineLauncher.py, Play.py et Create.py"""
    
import pygame
import os
import sys
import random


def button(screen, position, text):
    """crée un bouton"""
    font = pygame.font.Font("./Fonts/Super Mario Maker Extended.ttf", 75)
    text_render = font.render(text, 1, (10, 10, 51))
    x, y, w , h = text_render.get_rect()
    x, y = position
    pygame.draw.line(screen, (150, 150, 200), (x, y), (x + w , y), 5)
    pygame.draw.line(screen, (150, 150, 200), (x, y - 2), (x, y + h), 5)
    pygame.draw.line(screen, (50, 50, 100), (x, y + h), (x + w , y + h), 5)
    pygame.draw.line(screen, (50, 50, 100), (x + w , y+h), [x + w , y], 5)
    pygame.draw.rect(screen, (100, 100, 150), (x, y, w , h))
    return screen.blit(text_render, (x, y))


def create():
    """lance Create.py"""
    os.system(r"python Create.py")
    

def play():
    """lance Play.py"""
    os.system(r"python Play.py")
    
    
def world():
    """lance World.py"""
    os.system(r"python World.py")
    
    
def online():
    """lance OnlineLauncher.py"""
    os.system(r"python Online_Laucher.py")


def menu():
    """crée le menu et affiche les boutons"""
    pygame.init()
    screen = pygame.display.set_mode((1080, 720))
    screen.blit(pygame.image.load("./Sprites/background/bg_1.png"), (0,0))
    pygame.mixer.pre_init(44100, -16, 2, 2048)
    pygame.mixer.music.load("./Music/Jeremy Blake - Powerup !.mp3")
    b1 = button(screen, (130, 600), "Quitter")
    b2 = button(screen, (130, 400), "Créer")
    b3 = button(screen, (130, 300), "Jouer")
    b5 = button(screen, (130, 500), "Monde")
    b4 = button(screen, (130, 200), "Multijoueur")
    A = False
    while True:
        font = pygame.font.Font("./Fonts/Super Mario Maker Extended.ttf", 75)
        text_surface = font.render('Super Pablo Maker 2', False, (255, 189, 27))
        text_surface_2 = font.render('Super Pablo Maker 2', False, (0, 0, 0))
        text_surface_3 = font.render('NEW !', False, (100 + random.randint(0, 110), 25, 25))
        text_surface_3 = pygame.transform.rotate(text_surface_3, 30)
        screen.blit(text_surface_2, (140,65))
        screen.blit(text_surface, (150,75))
        screen.blit(text_surface_3, (0,0))
        if A == False:
            pygame.mixer.music.play(-1)
            A = True
        for event in pygame.event.get():
            if (event.type == pygame.QUIT):
                pygame.quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if b1.collidepoint(pygame.mouse.get_pos()):
                    pygame.quit()
                elif b2.collidepoint(pygame.mouse.get_pos()):
                    A = False
                    pygame.mixer.music.stop()
                    create()
                elif b3.collidepoint(pygame.mouse.get_pos()):
                    A = False
                    pygame.mixer.music.stop()
                    play()
                elif b4.collidepoint(pygame.mouse.get_pos()):
                    A = False
                    pygame.mixer.music.stop()
                    online()
                elif b5.collidepoint(pygame.mouse.get_pos()):
                    A = False
                    pygame.mixer.music.stop()
                    world()
        try:    
            pygame.display.update()
        except:
            exit()
    pygame.quit()
    

menu()