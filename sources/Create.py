import pygame
import os
import ast
import PySimpleGUI as sg
import keyboard
import tkinter
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import sys
from functools import wraps


class Bloc(pygame.sprite.Sprite):
    """classe qui gère les blocs"""
    def __init__(self, blocks, type_block, cx, cy, list_block_literral, rect_list, szbw):
        """initialise la classe bloc"""
        pygame.sprite.Sprite.__init__(self)
        self.type = type_block
        self.cx = cx
        self.cy = cy
        self.szbw = szbw
        self.old_img = None
        self.img = None
        list_block_literral.append(self)
    def draw_bloc(self, screen, blocks_cords_only, rect_list, blocks, dx, dy):
        """dessine le bloc"""
        self.screen = screen
        if self.img == None:
            self.img = pygame.image.load(self.type).convert_alpha()
            self.img = pygame.transform.scale(self.img, (self.szbw, self.szbw)).convert_alpha()
            self.old_img = self.img
            blocks_cords_only.append((self.cx, self.cy))
            blocks.append((self.cx, self.cy, self.img))
            self.rect = pygame.Rect(dx, dy, self.szbw, self.szbw)
            rect_list.append(self.rect)
    def check_col(self, list_block_literral, blocks_cords_only):
        """vérifie les colisions de blocs"""
        if self.old_img != self.img:
            self.old_img = self.img
        if self.type == "./Sprites/grass.png":
            if (self.cx, self.cy - self.szbw) in blocks_cords_only:
                self.type = "./Sprites/dirt.png"
                self.img = pygame.image.load(self.type).convert_alpha()
                return True
        if self.type == "./Sprites/damage/lava.png":
            if (self.cx, self.cy - self.szbw) in blocks_cords_only:
                self.type = "./Sprites/damage/lava_2.png"
                self.img = pygame.image.load(self.type).convert_alpha()
                return True
    def update(self, screen, blocks_cords_only, rect_list, blocks):
        """met à jour le sprite de certains bloc si il y a colision"""
        try:
            temp_index = blocks_cords_only.index((self.cx, self.cy))
            blocks.pop(temp_index)
            blocks_cords_only.pop(temp_index)
            rect_list.pop(temp_index)
        except:
            pass
        self.screen = screen
        if self.img != pygame.image.load(self.type).convert_alpha():
            self.img = pygame.image.load(self.type).convert_alpha()
            self.img = pygame.transform.scale(self.img, (self.szbw, self.szbw)).convert_alpha()
            blocks.append((self.cx, self.cy, self.img))
            blocks_cords_only.append((self.cx, self.cy))
            self.rect = pygame.Rect(self.cx, self.cy, self.szbw, self.szbw)
            rect_list.append(self.rect)
            self.old_img = self.img
    def kill(self):
        """'tue' le bloc"""
        del self
    def __del__(self):
        return None
    def __repr__(self):
        return f"{self.rect}, {self.type}"
    def __call__(self):
        return f"{self.rect}, {self.type}"


def quitting_to_main_menu():
    """quitte vers le menu principal"""
    exit()


def cache(func):
    """fonction cache qui sert de décorateur"""
    cache = {}
    @wraps(func)
    def wrapper(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key not in cache:
            cache[key] = func(*args, **kwargs)
        return cache[key]
    return wrapper


@cache
def Load(blocks, list_block_literral, surface, blocks_cords_only):
    """Permet de charger le niveau"""
    E = []
    blocks2 = []
    Tk().withdraw()
    filenameL = askopenfilename(filetypes=[("Fichiers textes", "*.txt")])
    if filenameL != "":
        try:
            with open(filenameL,"r") as Load_Save:   
                for line in Load_Save.readlines():
                    E.append(line)
            clold = E[-4]
            dxold = E[-3]
            dyold = E[-2]
            cl = (parse_tuple(clold))
            dx = int(E[-3])
            dy = int(E[-2])
            ennemies_list = E[-1]
            ennemies_list = ennemies_list[1:-2]
            cached = ""
            enn = []
            flag = False
            for elt in ennemies_list :
                if elt == "," or elt == " ":
                    if flag == False:
                        cached += elt
                elif elt != "'":
                    if elt == '(' and flag == True:
                        flag = False
                    if elt != ')':
                        cached += elt
                    else:
                        cached += elt
                        flag = True
                        enn.append(parse_tuple(cached))
                        cached = ''
            removing = []
            for enn2 in enn:
                if enn2 == None:
                    removing.append(enn2)
            for rem in removing:
                enn.remove(rem)
            for eltt in E:
                if eltt != clold and eltt != dxold and eltt != dyold and eltt != E[-1]:
                    Tpl = (parse_tuple(eltt))
                    sz = 693 // 16
                    cx = Tpl[0]
                    cy = Tpl[1]
                    typebloc = Tpl[2]
                    if os.path.exists(typebloc) != True:
                        raise Exception
                blocks2.append(Tpl)
            for eltt in enn:
                if eltt != clold and eltt != dxold and eltt != dyold:
                    Tpl = (parse_tuple(eltt))
                    sz = 693 // 16
                    cx = Tpl[0]
                    cy = Tpl[1]
                    typebloc = Tpl[2]
                    if os.path.exists(typebloc) != True:
                        raise Exception
                blocks2.append(Tpl)
            return dx, dy, cl, blocks2, enn
        except:
            sg.popup('Sauvegarde vide ou corrompue.', title='Erreur', button_color=('#A81B0C', '#FFFFFF'), background_color='#F47264', line_width=29, custom_text='Ok')
    else:
        sg.popup('Aucune sauvegarde entrée.', title='Erreur', button_color=('#A81B0C', '#FFFFFF'), background_color='#F47264', line_width=25, custom_text='Ok')
       
        
@cache    
def parse_tuple(string):
    """transforme une string en tuple"""
    try:
        s = ast.literal_eval(str(string))
        if type(s) == tuple:
            return s
    except:
        return
    

def update(sizebtwn, dx, dy, rects, bloc_type, screen):
    """Met a jour la pos de la souris"""
    x, y = pygame.mouse.get_pos()
    if x > 0 and x < 693 and y > 0 and y < 693:
        ix = x // sizebtwn
        iy = y // sizebtwn
        cx, cy = ix * sizebtwn, iy * sizebtwn
        cx += dx
        cy += dy
        return cx, cy
    else:
        cmpt = 0
        try:
            for rect in rects:
                if x > rect.x and x < rect.x + rect.width and y > rect.y and y < rect.y + rect.height:
                    bloc_type = listofblocks[cmpt]
                    return bloc_type
                else:
                    cmpt += 1
        except:
            return bloc_type
        return bloc_type


def drawgrid(w, rows, surface, flaga, colorbg):
    """dessine une grille"""
    if flaga == True:
        sizebtwn = w // rows 
        for i in range(0, w, sizebtwn):
            x, y = i, i
            pygame.draw.line(surface, (255,200,200), (x, 0), (x, w))
            pygame.draw.line(surface, (255,200,200), (0, y), (w, y))
    else:
        sizebtwn = w // rows 
        for i in range(0, w, sizebtwn):
            x, y = i, i
            pygame.draw.line(surface, (colorbg), (x, 0), (x, w))
            pygame.draw.line(surface, (colorbg), (0, y), (w, y))
    return -1


def draw(rects, surface, sizebtwn, type_bloc, blocks, colorbg, blocks_cords_only, list_block_literral, rect_list, dx, dy):
    """crée un/des blocs"""
    temp_index = 0
    if type(update(sizebtwn, -dx, -dy, rects, type_bloc, surface)) == type(""):
        type_bloc = update(sizebtwn, -dx, -dy, rects, type_bloc, surface)
        return type_bloc
    else:
        cx, cy = update(sizebtwn, -dx, -dy, rects, type_bloc, surface)
        if (cx, cy) in blocks_cords_only:
            try:
                temp_index = blocks_cords_only.index((cx, cy))
                blocks.pop(temp_index)
                blocks_cords_only.pop(temp_index)
                rect_list.pop(temp_index)
                list_block_literral[temp_index].kill()
                list_block_literral.pop(temp_index)
            except:
                pass
        bloc_temp = Bloc(blocks, type_bloc, cx, cy, list_block_literral, rect_list, sizebtwn)
        bloc_temp.draw_bloc(surface, blocks_cords_only, rect_list, blocks, cx, cy)
        bloc_temp.kill()
        number_spawn = []
        number_flag = []
        for elements in list_block_literral:
            c = list_block_literral.count(elements)
            if "spawn" in elements.type:
                number_spawn.append(elements)
            if "flag" in elements.type:
                number_flag.append(elements)
            if c > 1:
                while c > 1:
                    list_block_literral.remove(elements)
                    c = list_block_literral.count(elements)
        try:
            for k in range(len(number_spawn)):
                if number_spawn[k] != number_spawn[-1]:
                    list_block_literral.remove(number_spawn[k])
                    blocks.remove((number_spawn[k].cx, number_spawn[k].cy, number_spawn[k].img))
                    blocks_cords_only.remove((number_spawn[k].cx, number_spawn[k].cy))
            for j in range(len(number_flag)):
                if number_flag[k] != number_flag[-1]:
                    list_block_literral.remove(number_flag[k])
                    blocks.remove((number_flag[k].cx, number_flag[k].cy, number_flag[k].img))
                    blocks_cords_only.remove((number_flag[k].cx, number_flag[k].cy))
        except:
            pass
        for elements in blocks:
            c = blocks.count(elements)
            if c > 1:
                while c > 1:
                    blocks.remove(elements)
                    c = blocks.count(elements)
        for elements in blocks_cords_only:
            c = blocks_cords_only.count(elements)
            if c > 1:
                while c > 1:
                    blocks_cords_only.remove(elements)
                    c = blocks_cords_only.count(elements)
        return type_bloc
                
 
def undraw(surface, type_bloc, rects, sizebtwn, blocks, blocks_cords_only, list_block_literral, rect_list, dx, dy):
    """détruit un/des blocs"""
    temp_index = 0
    if type(update(sizebtwn, -dx, -dy, rects, type_bloc, surface)) != str():
        try:
            cx, cy = update(sizebtwn, -dx, -dy, rects, type_bloc, surface)
            if ((cx, cy)) in blocks_cords_only:
                temp_index = blocks_cords_only.index((cx, cy))
                blocks.pop(temp_index)
                blocks_cords_only.pop(temp_index)
                rect_list.pop(temp_index)
                list_block_literral[temp_index].kill()
                list_block_literral.pop(temp_index)
        except:
            pass
    return -1


@cache
def draw_load(surface, sizebtwn, type_bloc, blocks, blocks_cords_only, list_block_literral, cx, cy, rect_list, dx, dy):
    """charge les blocs"""
    if (cx, cy) in blocks_cords_only:
        try:
            temp_index = blocks_cords_only.index((cx, cy))
            blocks.pop(temp_index)
            blocks_cords_only.pop(temp_index)
            rect_list.pop(temp_index)
            list_block_literral[temp_index].kill()
            list_block_literral.pop(temp_index)
        except:
            pass
    bloc_temp = Bloc(blocks, type_bloc, cx, cy, list_block_literral, rect_list, sizebtwn)
    bloc_temp.draw_bloc(surface, blocks_cords_only, rect_list, blocks, cx, cy)
    bloc_temp.kill()
    bloc_temp = None
    return -1
    

def drawontop(surface, sizebtwn, blocks, colorbg, list_block_literral, blocks_cords_only, dx, dy, rect_list):
    """dessine les blocs"""
    c = 0
    for elmt in list_block_literral:
        if elmt.check_col(list_block_literral, blocks_cords_only) == True and elmt.cx + dx >= 0 and elmt.cx + dx <= 693 and elmt.cy + dy >= 0 and elmt.cy + dy <= 693:
            elmt.update(surface, blocks_cords_only, rect_list, blocks)
    for elmts in blocks:
        c = blocks.count(elmts)
        if c > 1 :
            blocks.remove(elmts)
    for elmts in blocks_cords_only:
        c = blocks_cords_only.count(elmts)
        if c > 1 :
            blocks_cords_only.remove(elmts)
    for elmts in rect_list:
        c = rect_list.count(elmts)
        if c > 1 :
            rect_list.remove(elmts)
    for elt in blocks:
        local_cx = 0
        local_cy = 0
        if elt != colorbg:
            local_cx = elt[0] + dx
            local_cy = elt[1] + dy
            local_img = elt[2]
            if local_cx >= 0 and local_cy >= 0 and local_cx <= 693 and local_cy <= 693:
                surface.blit(local_img, (local_cx,local_cy))
    return -1


@cache
def ObtainSprites(directory):
        """obtiens les différents blocs à placer"""
        l = []
        path = ''
        for letters in directory:
            if letters == '/':
                path += '\\'
            elif letters != '.':
                path += letters
        for elt in os.listdir(os.getcwd()+path+'\\'):
            if elt.endswith(".png"):
                l.append('.'+directory+elt)
        return l


def main():
    """programme principal"""
    try:
        sys.setswitchinterval(500)
        pygame.mixer.pre_init(44100, -16, 2, 2048)
        pygame.init()
        pygame.mixer.music.load("./Music/Eric Skiff - Underclocked (underunderclocked mix).mp3")
        screen = pygame.display.set_mode((1080, 693), pygame.DOUBLEBUF, 8)
        clock = pygame.time.Clock()
        fps_font = pygame.font.SysFont((), 64)
        rect_list = []
        List_To_Draw = []
        list_block_literral = []
        blocks = []
        blocks_cords_only = []
        insts = None
        rects = []
        menu_x,menu_y = 10, 10
        global listofblocks
        listofblocks = []
        for elt in ObtainSprites("/Sprites/"):
            if elt != "./Sprites/empty.png/":
                listofblocks.append(elt)
        for elt2 in ObtainSprites("/Sprites/special/"):
            listofblocks.append(elt2)
        for elt3 in ObtainSprites("/Sprites/damage/"):
            if "lava_2" not in elt3:
                listofblocks.append(elt3)
        for eltt in ["angel", "fly", "turret", "jeanpascaldelaflibustiere", "cactus", "penguini", "items"]:
            if eltt == "angel":
                listofblocks.append("./Sprites/ennemy/angel/angel.png")
            elif eltt == "items":
                for elmt in ObtainSprites("/Sprites/ennemy/items/"):
                    listofblocks.append(elmt)
            elif eltt == "jeanpascaldelaflibustiere":
                listofblocks.append("./Sprites/ennemy/" + eltt + "/phase1/idle/idle.png")
            else:
                listofblocks.append("./Sprites/ennemy/" + eltt + "/idle/idle.png")
        for elt3 in ObtainSprites("/Sprites/ennemy/"):
            listofblocks.append(elt3)
        menu = pygame.Surface((387, 693))
        menu.fill((100,100,200))
        for sprites in listofblocks:
            menu.blit(pygame.transform.scale(pygame.image.load(sprites).convert_alpha(), (693 // 20, 693 // 20)), (menu_x, menu_y))
            if listofblocks.index(sprites) == 0:
                if pygame.Rect(703, menu_y, 693 // 20, 693 // 20) not in rects:
                    rects.append(pygame.Rect(703, menu_y, 693 // 20, 693 // 20))
            else:
                if pygame.Rect(703 + menu_x, menu_y, 693 // 20, 693 // 20) not in rects:
                    rects.append(pygame.Rect(703 + menu_x, menu_y, 693 // 20, 693 // 20))
            if menu_x >= 271:
                menu_y += 50
                menu_x = 10
            else:
                menu_x += 40
        menu_x, menu_y = 10, 10
        ennemies_list = []
        dx, dy = 0, 0
        bloc_type = "./Sprites/grass.png"
        flaga = True
        run = True
        eventC, valuesC = sg.Window(' ', [[sg.Text('Voulez-vous charger la sauvegarde ?')],[sg.Button('Oui'), sg.Button('Non')]]).read(close=True)
        if eventC == 'Oui':
            tempload = Load(blocks, list_block_literral, screen, blocks_cords_only)
            if tempload != None:
                dx, dy, colorbg, List_To_Draw, ennemies_list = tempload
                for elt in ennemies_list:
                    List_To_Draw.append(elt)
            else:
                colorbg = None
        else:
            colorbg = None
        Turn1 = True
        while run:
            clock.tick(30)
            fps_surface = fps_font.render(clock.get_fps().__int__().__str__(), False, (100,50,50))
            if colorbg == None:
                colorbg = (0,0,0)
            screen.fill(colorbg)
            drawgrid(693, 16, screen, flaga, colorbg)
            drawontop(screen, 693 // 16, blocks, colorbg, list_block_literral, blocks_cords_only, dx, dy, rect_list)
            if Turn1 == True:
                pygame.mixer.music.play(-1)
                for elt in List_To_Draw:
                    if elt != colorbg:
                        draw_load(screen, 693 // 16, elt[2], blocks, blocks_cords_only, list_block_literral, elt[0], elt[1], rect_list, dx, dy)
                List_To_Draw = None
            Turn1 = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_g:
                        if flaga == True:
                            flaga = False
                        else:
                            flaga = True
                        print(flaga)
                    if event.key == pygame.K_b:
                        eventColorbg, valuesColorbg = sg.Window("Choix de l'arrière plan", [[sg.Text('')],[sg.Button('Bleu Ciel')], [sg.Button('Bleu Nuit')], [sg.Button('Noir')], [sg.Button('Orange')], [sg.Button('Jaune')], [sg.Button('Vert foncé')], [sg.Button('Vert clair')]]).read(close=True)
                        if eventColorbg == "Bleu Ciel":
                            colorbg = (139,184,241)
                        elif eventColorbg == "Bleu Nuit":
                            colorbg = (43,44,90)
                        elif eventColorbg == "Noir":
                            colorbg = (0,0,0)
                        elif eventColorbg == "Orange":
                            colorbg = (234,192,150)
                        elif eventColorbg == "Jaune":
                            colorbg = (240,215,153)
                        elif eventColorbg == "Vert foncé":
                            colorbg = (2,107,84)
                        elif eventColorbg == "Vert clair":
                            colorbg = (185,208,128)
                        if eventColorbg != None:
                            screen.fill(colorbg)
                            drawgrid(693, 16, screen, flaga, colorbg)
                            drawontop(screen, 693 // 16, blocks, colorbg, list_block_literral, blocks_cords_only, dx, dy, rect_list)
                    if event.key == pygame.K_UP:
                        dy += 693 // 16
                    if event.key == pygame.K_DOWN:
                        dy -= 693 // 16
                    if event.key == pygame.K_LEFT:
                        dx += 693 // 16
                    if event.key == pygame.K_RIGHT:
                        dx -= 693 // 16
                    if event.key == pygame.K_g:
                        if flaga == True:
                            flaga = False
                        else:
                            flaga = True
                    if event.key == pygame.K_k:
                        raise Exception
                    if event.key == pygame.K_s:
                        eventS, valuesS = sg.Window(' ', [[sg.Text('Voulez-vous sauvegarder ?')],[sg.Button('Oui'), sg.Button('Non')]]).read(close=True)
                        if eventS == 'Oui':
                            Tk().withdraw()
                            filenameS = askopenfilename(filetypes=[("Fichiers textes", "*.txt")])
                            if filenameS != "":
                                Saves = open(filenameS,"w+")
                                Saves.truncate()
                                TEMP_Save = ""
                                spawnpoint = '(' + str(0) + ', ' + str(0) + ', ' + "\"" + "./Sprites/special/spawn.png" + "\"" + ')' + "\n"
                                for elt in list_block_literral:
                                    if "ennemy" not in elt.type and "spawn" not in elt.type:
                                        TEMP_Save += '(' + str(elt.cx) + ', ' + str(elt.cy) + ', ' + "\"" + str(elt.type) + "\"" + ')' + "\n"
                                    else:
                                        if "spawn" not in elt.type:
                                            ennemies_list.append(('(' + str(elt.cx) + ', ' + str(elt.cy) + ', ' + "\"" + str(elt.type) + "\"" + ')'))
                                        else:
                                            spawnpoint = elt
                                if type(spawnpoint) == type(""):
                                    TEMP_Save += spawnpoint
                                else:
                                    TEMP_Save += '(' + str(spawnpoint.cx) + ', ' + str(spawnpoint.cy) + ', ' + "\"" + str(spawnpoint.type) + "\"" + ')' + "\n"
                                TEMP_Save += str(colorbg) + "\n"
                                TEMP_Save += str(dx) + "\n"
                                TEMP_Save += str(dy) + "\n"
                                TEMP_Save += str(ennemies_list) + "\n"
                                Saves.writelines(TEMP_Save)
                                Saves.close()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    click = pygame.mouse.get_pressed()
                    if click[0]:
                        bloc_type = draw(rects, screen, 693 // 16, bloc_type, blocks, colorbg, blocks_cords_only, list_block_literral, rect_list, dx, dy)
                    elif click[2]:
                        undraw(screen, bloc_type, rects, 693 // 16, blocks, blocks_cords_only, list_block_literral, rect_list, dx, dy)
            screen.blit(fps_surface, (10,10))
            screen.blit(menu, (693, 0))
            pygame.display.flip()
        eventS, valuesS = sg.Window(' ', [[sg.Text('Voulez-vous sauvegarder ?')],[sg.Button('Oui'), sg.Button('Non')]]).read(close=False)
        if eventS == 'Oui':
            Tk().withdraw()
            filenameS = askopenfilename(filetypes=[("Fichiers textes", "*.txt")])
            if filenameS != "":
                Saves = open(filenameS,"w+")
                Saves.truncate()
                TEMP_Save = ""
                spawnpoint = '(' + str(0) + ', ' + str(0) + ', ' + "\"" + "./Sprites/special/spawn.png" + "\"" + ')' + "\n"
                for elt in list_block_literral:
                    if "ennemy" not in elt.type and "spawn" not in elt.type:
                        TEMP_Save += '(' + str(elt.cx) + ', ' + str(elt.cy) + ', ' + "\"" + str(elt.type) + "\"" + ')' + "\n"
                    else:
                        if "spawn" not in elt.type:
                            ennemies_list.append(('(' + str(elt.cx) + ', ' + str(elt.cy) + ', ' + "\"" + str(elt.type) + "\"" + ')'))
                        else:
                            spawnpoint = elt
                if type(spawnpoint) == type(""):
                    TEMP_Save += spawnpoint
                else:
                    TEMP_Save += '(' + str(spawnpoint.cx) + ', ' + str(spawnpoint.cy) + ', ' + "\"" + str(spawnpoint.type) + "\"" + ')' + "\n"
                TEMP_Save += str(colorbg) + "\n"
                TEMP_Save += str(dx) + "\n"
                TEMP_Save += str(dy) + "\n"
                TEMP_Save += str(ennemies_list) + "\n"
                Saves.writelines(TEMP_Save)
                Saves.close()
        else:
            quitting_to_main_menu()
        quitting_to_main_menu()
    except Exception as e:
        eventS, valuesS = sg.Window(' ', [[sg.Text('Le jeu à rencontré une erreur et à planté.' + ' Erreur rencontrée : ' + str(e) + '. Voulez-vous sauvegarder ?')],[sg.Button('Oui'), sg.Button('Non')]]).read(close=False)
        if eventS == 'Oui':
            Tk().withdraw()
            filenameS = askopenfilename(filetypes=[("Fichiers textes", "*.txt")])
            if filenameS != "":
                Saves = open(filenameS,"w+")
                Saves.truncate()
                TEMP_Save = ""
                spawnpoint = '(' + str(0) + ', ' + str(0) + ', ' + "\"" + "./Sprites/special/spawn.png" + "\"" + ')' + "\n"
                for elt in list_block_literral:
                    if "ennemy" not in elt.type and "spawn" not in elt.type:
                        TEMP_Save += '(' + str(elt.cx) + ', ' + str(elt.cy) + ', ' + "\"" + str(elt.type) + "\"" + ')' + "\n"
                    else:
                        if "spawn" not in elt.type:
                            ennemies_list.append(('(' + str(elt.cx) + ', ' + str(elt.cy) + ', ' + "\"" + str(elt.type) + "\"" + ')'))
                        else:
                            spawnpoint = elt
                if type(spawnpoint) == type(""):
                    TEMP_Save += spawnpoint
                else:
                    TEMP_Save += '(' + str(spawnpoint.cx) + ', ' + str(spawnpoint.cy) + ', ' + "\"" + str(spawnpoint.type) + "\"" + ')' + "\n"
                TEMP_Save += str(colorbg) + "\n"
                TEMP_Save += str(dx) + "\n"
                TEMP_Save += str(dy) + "\n"
                TEMP_Save += str(ennemies_list) + "\n"
                Saves.writelines(TEMP_Save)
                Saves.close()
        quitting_to_main_menu()


main()