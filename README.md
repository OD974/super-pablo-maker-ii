# Utilisation du Mode Play


## Initialisation

Assurez-vous d'avoir installé les dépendances requises  

```bash
pip install -r requirements.txt
```

## Usage

Lancez ```Launcher.py``` et cliquez sur jouer

Une fenêtre s'ouvre vous demandant de choisir un niveau
Cliquez sur oui, puis sélectionnez votre fichier ```.txt```. Une fois cela fait, si le fichier que vous avec choisi est valide, alors vous pourrez jouer au niveau !

## Commandes

[&#8592;] : Aller vers la gauche                   
[&#8593;] : Sauter                
[&#8594;] : Aller vers la droite  
[Clic gauche] : Utiliser une capacité

# Utilisation du Mode Create




## Usage

Lancez ```Launcher.py``` et cliquez sur créer.

Une fenêtre s'ouvre vous demandant si vous voulez charger un niveau.
Si oui, sélectionnez un fichier ```.txt``` ou créez en un. Une fois cela fait, si le fichier que vous avec choisi est valide, alors vous pourrez l'éditer

## Commandes

[&#8592;] : Aller vers la gauche                   
[&#8593;] : Aller vers le haut          
[&#8594;] : Aller vers la droite  
[&#8594;] : Aller vers la bas  
[Clic gauche] : Placer un bloc  
[Clic droit] : Détruire un bloc
[G] : Choisir un arrière-plan
[K] : Provoquer un crash

# Utilisation du Mode Host



Il faudra que vous fournissier à la personne qui vous rejoint une ip locale (ou l'ip que fournit [Hamachi](https://vpn.net))

## Usage

Lancez ```Launcher.py``` et cliquez sur Multijoueur, puis cliquez sur Héberger une partie


Vous devrez attendre que quelqu'un se connecte. Puis une fenêtre s'ouvrira vous demandant de choisir un niveau
Cliquez sur oui, puis sélectionnez le même fichier ```.txt``` qu'a mis la personne qui vous a rejoint. Une fois cela fait, si le fichier que vous avec choisi est valide, alors vous pourrez jouer au niveau en multijoueur !

## Commandes

[&#8592;] : Aller vers la gauche                   
[&#8593;] : Sauter                
[&#8594;] : Aller vers la droite  
[Clic gauche] : Utiliser une capacité

# Utilisation du Mode Client


## Initialisation

Assurez-vous d'avoir installé les dépendances requises  

Il faudra que vous rentriez l'ip locale de la personne qui héberge la partie à la ligne 1335   
s.connect(L'ip de l'hote sous le format suivant : "w.x.y.z") (ou l'ip que fournit [Hamachi](https://vpn.net))

## Usage

Lancez ```Launcher.py``` et cliquez sur Multijoueur, puis cliquez sur Rejoindre une partie


Une fenêtre s'ouvrira vous demandant de choisir un niveau
Cliquez sur oui, puis sélectionnez le même fichier ```.txt``` que vas mettre la personne qui va vous a rejoindre. Une fois cela fait, si le fichier que vous avec choisi est valide, alors vous devrez patientez que l'hôte se connecte puis vous pourrez jouer au niveau en multijoueur !

## Commandes

[&#8592;] : Aller vers la gauche                   
[&#8593;] : Sauter                
[&#8594;] : Aller vers la droite  
[Clic gauche] : Utiliser une capacité

# Utilisation du Mode World

## Initialisation

Assurez-vous d'avoir installé les dépendances requises  


## Usage

Lancez ```Launcher.py``` et cliquez sur jouer

## Commandes

[&#8592;] : Aller vers la gauche                   
[&#8593;] : Sauter                
[&#8594;] : Aller vers la droite  
[Clic gauche] : Utiliser une capacité